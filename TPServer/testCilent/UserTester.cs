﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Client
{
    class UserTester
    {

        public string RunTest()
        {
            int choice = 0;
            Console.WriteLine("Choose one option:");
            Console.WriteLine("1.add player");
            Console.WriteLine("2.login");
            Console.WriteLine("3.delete player");
            Console.WriteLine("4.update player");
            Console.WriteLine("5.add court");
            Console.WriteLine("6.add court review");
            Console.WriteLine("7.add game");
            Console.WriteLine("8.delete game");
            //Console.WriteLine("9.update game");
            Console.WriteLine("10.add admin");
            Console.WriteLine("11.delete admin");
            Console.WriteLine("12.update court");
            Console.WriteLine("13.delete court");
            Console.WriteLine("14.end test");

            choice = Int32.Parse(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    return AddPlayer();
                case 2:
                    return Login();
                case 3:
                    return DeletePlayer();
                case 4:
                    return UpdatePlayer();
                case 5:
                    return AddCourt();
                case 6:
                    return AddCourtReview();
                case 7:
                    return AddNewGame();
                case 8:
                    return DeleteGame();
                case 9:
                    return "not implemented";
                case 10:
                    return AddAdmin();
                case 11:
                    return DeleteAdmin();
                case 12:
                    return UpdateCourt();
                case 13:
                    return DeleteCourt();
                case 14:
                    return "exit";
                default:
                    return "exit";
            }
        }

        public string AddPlayer()
        {
            Dictionary<string, string> parms = new Dictionary<string, string>();

            Console.WriteLine("name:");
            String name = Console.ReadLine();
            Console.WriteLine("last name:");
            String last = Console.ReadLine();
            Console.WriteLine("email:");
            String mail = Console.ReadLine();
            Console.WriteLine("username:");
            String username = Console.ReadLine();
            Console.WriteLine("password:");
            String password = Console.ReadLine();
            int c;
            String birthdatestring;
            do
            {
                Console.WriteLine("insert birthdate");
                Console.WriteLine("dd/mm/yyyy");
                birthdatestring = Console.ReadLine();
                c = birthdatestring.Count(f => f == '/');
            } while (c != 2);
            string path = "/example/path/to/image";
            Console.WriteLine("insert city");
            String city = Console.ReadLine();
            parms.Add("username", username);
            parms.Add("password", password);
            parms.Add("email", mail);
            parms.Add("firstName", name);
            parms.Add("lastName", last);
            parms.Add("city", city);
            parms.Add("birthday", birthdatestring);
            JObject request = CreateRequest("UL1", parms);
            return request.ToString();
        }

        private string Login()
        {
            Dictionary<string, string> parms = new Dictionary<string, string>();
            Console.WriteLine("insert username");
            string username = Console.ReadLine();
            Console.WriteLine("insert password");
            string password = Console.ReadLine();
            parms.Add("username", username);
            parms.Add("password", password);
            return CreateRequest("UL2", parms).ToString();
        }

        public string DeletePlayer()
        {
            Dictionary<string, string> parms = new Dictionary<string, string>();
            Console.WriteLine("insert uid to delete:");
            string uid = Console.ReadLine();
            Console.WriteLine("are you admin? Y/N:");
            string isAdmin = Console.ReadLine();
            string admin = "";
            if (isAdmin.ToLower().Equals("y")){
                Console.WriteLine("insert username:");
                admin = Console.ReadLine();
            }
            Console.WriteLine("insert password:");
            string password = Console.ReadLine();
            parms.Add("uid", uid);
            parms.Add("password", password);
            if(!admin.Equals(string.Empty))
                parms.Add("admin", admin);
            JObject request = CreateRequest("UP2", parms);
            return request.ToString();
        }

        public string UpdatePlayer()
        {
            Dictionary<string, string> parms = new Dictionary<string, string>();
            Console.WriteLine("insert uid to update:");
            uint uid = Convert.ToUInt32(Console.ReadLine());
            parms.Add("uid", uid.ToString());
            Console.WriteLine("change email? Y/N");
            if (Console.ReadLine().ToLower().Equals("y"))
            {
                Console.WriteLine("insert email:");
                parms.Add("email", Console.ReadLine());
            }
            Console.WriteLine("change password? Y/N");
            if (Console.ReadLine().ToLower().Equals("y"))
            {
                Console.WriteLine("insert old:");
                parms.Add("oldPassword", Console.ReadLine());
                Console.WriteLine("insert new:");
                parms.Add("newPassword", Console.ReadLine());
            }
            Console.WriteLine("change name? Y/N");
            if (Console.ReadLine().ToLower().Equals("y"))
            {
                Console.WriteLine("insert new name:");
                parms.Add("name", Console.ReadLine());
            }
            Console.WriteLine("change last name? Y/N");
            if (Console.ReadLine().ToLower().Equals("y"))
            {
                Console.WriteLine("insert new last name:");
                parms.Add("lastName", Console.ReadLine());
            }
            Console.WriteLine("change city? Y/N");
            if (Console.ReadLine().ToLower().Equals("y"))
            {
                Console.WriteLine("insert city:");
                parms.Add("city", Console.ReadLine());
            }
            Console.WriteLine("change birthdate? Y/N");
            if (Console.ReadLine().ToLower().Equals("y"))
            {
                int c;
                String birthdatestring;
                do
                {
                    Console.WriteLine("insert birthdate");
                    Console.WriteLine("dd/mm/yyyy");
                    birthdatestring = Console.ReadLine();
                    c = birthdatestring.Count(f => f == '/');
                } while (c != 2);
                parms.Add("birthdate", birthdatestring);
            }
            JObject request = CreateRequest("UP1", parms);
            return request.ToString();
        }

        public string AddCourt()
        {
            Dictionary<string, string> parms = new Dictionary<string, string>();

            Console.WriteLine("name:");
            parms.Add("name", Console.ReadLine());

            Console.WriteLine("city:");
            parms.Add("city", Console.ReadLine());

            Console.WriteLine("enter street? Y/N");
            if (Console.ReadLine().ToLower().Equals("y"))
            {
                Console.WriteLine("insert street:");
                parms.Add("street", Console.ReadLine());
            }
            Console.WriteLine("enter street number? Y/N");
            if (Console.ReadLine().ToLower().Equals("y"))
            {
                Console.WriteLine("insert street num:");
                parms.Add("streetNum", Console.ReadLine());
            }
            Console.WriteLine("enter kind? Y/N");
            if (Console.ReadLine().ToLower().Equals("y"))
            {
                Console.WriteLine("insert kind:");
                parms.Add("kind", Console.ReadLine());
            }
            Console.WriteLine("enter width? Y/N");
            if (Console.ReadLine().ToLower().Equals("y"))
            {
                Console.WriteLine("insert width:");
                parms.Add("width", Console.ReadLine());
            }
            Console.WriteLine("enter length? Y/N");
            if (Console.ReadLine().ToLower().Equals("y"))
            {
                Console.WriteLine("insert length:");
                parms.Add("length", Console.ReadLine());
            }
            Console.WriteLine("enter description? Y/N");
            if (Console.ReadLine().ToLower().Equals("y"))
            {
                Console.WriteLine("insert description:");
                parms.Add("description", Console.ReadLine());
            }
            Console.WriteLine("enter phone? Y/N");
            if (Console.ReadLine().ToLower().Equals("y"))
            {
                Console.WriteLine("insert phone:");
                parms.Add("phone", Console.ReadLine());
            }

            JObject request = CreateRequest("CU1", parms);
            return request.ToString();
        }

        private string AddCourtReview()
        {
            Dictionary<string, string> parms = new Dictionary<string, string>();
            Console.WriteLine("insert court id:");
            parms.Add("courtId", Console.ReadLine());
            Console.WriteLine("insert rank:");
            parms.Add("rank", Console.ReadLine());
            Console.WriteLine("insert ranker id:");
            parms.Add("rankerId", Console.ReadLine());
            Console.WriteLine("add text? Y/N");
            if (Console.ReadLine().ToLower().Equals("y"))
            {
                Console.WriteLine("insert text:");
                parms.Add("text", Console.ReadLine());
            }
            return CreateRequest("CU4", parms).ToString();
        }

        private string AddNewGame()
        {
            Dictionary<string, string> parms = new Dictionary<string, string>();
            Console.WriteLine("insert user id:");
            parms.Add("creatorId", Console.ReadLine());
            Console.WriteLine("insert court Id:");
            parms.Add("courtId", Console.ReadLine());
            Console.WriteLine("insert date:");
            parms.Add("date", Console.ReadLine());
            Console.WriteLine("insert time:");
            parms.Add("time", Console.ReadLine());
            Console.WriteLine("number of players? Y/N:");
            if (Console.ReadLine().ToLower().Equals("y"))
            {
                Console.WriteLine("insert num of players:");
                parms.Add("playerNumber", Console.ReadLine());
            }
            Console.WriteLine("insert private? Y/N:");
            if (Console.ReadLine().ToLower().Equals("y"))
            {
                Console.WriteLine("insert private TRUE/FALSE:");
                parms.Add("private", Console.ReadLine());
            }
            Console.WriteLine("insert duration? Y/N:");
            if (Console.ReadLine().ToLower().Equals("y"))
            {
                Console.WriteLine("insert duration in minutes:");
                parms.Add("duration", Console.ReadLine());
            }
            return CreateRequest("GU1", parms).ToString();
        }

        private string DeleteGame()
        {
            Dictionary<string, string> parms = new Dictionary<string, string>();
            Console.WriteLine("insert game id:");
            parms.Add("gameId", Console.ReadLine());
            return CreateRequest("GU3", parms).ToString();
        }

        public string AddAdmin()
        {
            Dictionary<string, string> parms = new Dictionary<string, string>();
            Console.WriteLine("username:");
            String username = Console.ReadLine();
            Console.WriteLine("password:");
            String password = Console.ReadLine();
            Console.WriteLine("enter admin's email:");
            String mail = Console.ReadLine();
            Console.WriteLine("enter admin's username:");
            String Ausername = Console.ReadLine();
            Console.WriteLine("enter admin's password:");
            String Apassword = Console.ReadLine();
            Console.WriteLine("enter admin's permission:");
            String permission = Console.ReadLine();
            parms.Add("username", username);
            parms.Add("password", password);
            parms.Add("AUsername", Ausername);
            parms.Add("APassword", Apassword);
            parms.Add("Aemail", mail);
            parms.Add("Apermission", permission);
            JObject request = CreateRequest("UA1", parms);
           return request.ToString();
        }

        private string DeleteAdmin()
        {
            Dictionary<string, string> parms = new Dictionary<string, string>();
            Console.WriteLine("insert admin's id:");
            parms.Add("uid", Console.ReadLine());
            Console.WriteLine("insert admin's username who requested this deletion:");
            parms.Add("admin", Console.ReadLine());
            Console.WriteLine("insert admin's password:");
            parms.Add("password", Console.ReadLine());
            return CreateRequest("UA2", parms).ToString();
        }

        public string UpdateCourt()
        {
            Dictionary<string, string> parms = new Dictionary<string, string>();
            Console.WriteLine("insert uid to update:");
            uint uid = Convert.ToUInt32(Console.ReadLine());
            parms.Add("uid", uid.ToString());
            Console.WriteLine("change name? Y/N");
            if (Console.ReadLine().ToLower().Equals("y"))
            {
                Console.WriteLine("insert new name:");
                parms.Add("name", Console.ReadLine());
            }
            Console.WriteLine("change street? Y/N");
            if (Console.ReadLine().ToLower().Equals("y"))
            {
                Console.WriteLine("insert new street:");
                parms.Add("street", Console.ReadLine());
            }
            Console.WriteLine("change streetNum? Y/N");
            if (Console.ReadLine().ToLower().Equals("y"))
            {
                Console.WriteLine("insert new streetNum:");
                parms.Add("streetNum", Console.ReadLine());
            }
            Console.WriteLine("change kind? Y/N");
            if (Console.ReadLine().ToLower().Equals("y"))
            {
                Console.WriteLine("insert new kind:");
                parms.Add("kind", Console.ReadLine());
            }
            Console.WriteLine("change width? Y/N");
            if (Console.ReadLine().ToLower().Equals("y"))
            {
                Console.WriteLine("insert width:");
                parms.Add("width", Console.ReadLine());
            }
            Console.WriteLine("change length? Y/N");
            if (Console.ReadLine().ToLower().Equals("y"))
            {
                Console.WriteLine("insert length:");
                parms.Add("length", Console.ReadLine());
            }
            Console.WriteLine("change description? Y/N");
            if (Console.ReadLine().ToLower().Equals("y"))
            {
                Console.WriteLine("insert description:");
                parms.Add("description", Console.ReadLine());
            }
            Console.WriteLine("change phone? Y/N");
            if (Console.ReadLine().ToLower().Equals("y"))
            {
                Console.WriteLine("insert phone:");
                parms.Add("phone", Console.ReadLine());
            }
            JObject request = CreateRequest("CU2", parms);
            return request.ToString();
        }

        private string DeleteCourt()
        {
            Dictionary<string, string> parms = new Dictionary<string, string>();
            Console.WriteLine("insert court's id to delete:");
            parms.Add("uid", Console.ReadLine());
            Console.WriteLine("insert admin's username who requested this deletion:");
            parms.Add("admin", Console.ReadLine());
            Console.WriteLine("insert admin's password:");
            parms.Add("password", Console.ReadLine());
            return CreateRequest("CU3", parms).ToString();
        }

        private JObject CreateRequest(string command, Dictionary<string, string> parms)
        {
            JObject json = new JObject();
            json.Add("command", command);
            JObject parmsObject = new JObject();
            foreach (KeyValuePair<string, string> entry in parms)
            {
                parmsObject.Add(entry.Key, entry.Value);
            }
            json.Add("parms", parmsObject);
            return json;
        }

 

    }
}
