﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class Client
    {
        public Socket server { set; get; }

        /// <summary>
        /// responsible for sending client's input to server.</summary>
        public void GetInput()
        {
            UserTester test = new UserTester();
            while (true)
            {
                string input = test.RunTest();
                if (input == "exit")
                    break;
                server.Send(Encoding.ASCII.GetBytes(input));
            }
            server.Shutdown(SocketShutdown.Both);
            server.Close();
        }

        /// <summary>
        ///getResult function is responsible for getting server's rosponse.</summary>
        public void GetResult()
        {
            while (server.Connected)
            {
                byte[] data = new byte[1024];
                int recv;
                try
                {
                    recv = server.Receive(data);
                }
                catch (Exception e) { break; }
                string stringData = Encoding.ASCII.GetString(data, 0, recv);
                Console.WriteLine(stringData);
            }
        }
    }
}
