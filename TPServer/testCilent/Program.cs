﻿
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Configuration;


namespace Client
{
    /// <summary>
    /// Class Program creates new client.</summary>
    class Program
    {
        /// <summary>
        /// main</summary>
        public static void Main(string[] args)
        {
            //get ip and port from app.config file.
            string IP = ConfigurationManager.AppSettings["ip"];
            int PORT = Int32.Parse(ConfigurationManager.AppSettings["port"]);
            IPEndPoint ipep = new IPEndPoint(IPAddress.Parse(IP), PORT);
            Client client = new Client();
            client.server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                client.server.Connect(ipep);

                Thread input = new Thread(client.GetInput);
                input.Start();
                Thread output = new Thread(client.GetResult);
                output.Start();

            }
            catch (SocketException e) { Console.WriteLine("Unable to connect to server." + e.ToString()); }
        }
    }
}
