﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPServer;

namespace Server.Commands
{
    public class GetAllCourtsCommand:Command
    {
        public GetAllCourtsCommand() { }

        public override Command Clone()
        {
            return new GetAllCourtsCommand();
        }

        public override void Execute()
        {
            DBConnector DB = new DBConnector();
            List<Court> courts = DB.GetAllCourtsFromDB();
            //correct username and password - can delete user.
            if (courts != null)
            {
                JArray jarray = new JArray();
                foreach (Court court in courts){
                    JObject json = new JObject();
                    json.Add("id", court.Id);
                    json.Add("name", court.Name);
                    json.Add("city", court.City);
                    json.Add("street", court.Street);
                    json.Add("kind", court.kind);
                    json.Add("streetNum", court.StreetNumber);
                    json.Add("width", court.width);
                    json.Add("length", court.length);
                    json.Add("LA", court.LA);
                    json.Add("LO", court.LO);
                    json.Add("description", court.description);
                    jarray.Add(json);
                }
                
                result = CreateAnswer("CA1", "success", jarray.ToString());
            }
            else
            {
                result = CreateAnswer("CA1", "err4", string.Empty);
            }
        }
    }
}
