﻿
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System;

namespace Server.Commands
{
    ///<summary> abstract Class Command represents a command to execute.</summary>
    public abstract class Command : ICommand
    {
        public JObject result { get; set; }
        public Dictionary<string, string> param { set; get; }
        public Presenter caller { get; set; }

        //will hold all the parameters needed for this command to be executed
        protected List<string> paramNames = new List<string>();

        ///<summary> Clone function returns new instance of this command</summary>
        public abstract Command Clone();
        ///<summary> Execute function execute this command</summary>
        public abstract void Execute();

        ///<summary> will keep in paramNames only missing parameters </summary>
        protected bool IsAllParamsExist()
        {
            List<string> tmpNames = new List<string>(paramNames);
            foreach (var parameter in tmpNames)
            {
                if (param.ContainsKey(parameter))
                {
                    paramNames.Remove(parameter);
                }
            }

            if (paramNames.Count == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected JObject CreateAnswer(string type, string status, string content)
        {
            JObject json = new JObject();
            json.Add("type", type);
            JObject answerObject = new JObject();
            answerObject.Add("status", status);
            answerObject.Add("content", content);
            json.Add("answer", answerObject);
            return json;
        }

        protected JObject CreateMissingParamsAns(string type)
        {
            string missing = string.Empty;
            foreach (string paramName in paramNames)
            {
                missing += paramName + ";";
            }
            return CreateAnswer(type, "err1", missing);
        }

    }
}
