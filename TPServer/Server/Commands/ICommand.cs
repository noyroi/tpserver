﻿namespace Server.Commands
{
    ///<summary> interface represents all kinds of Commands.</summary>
    public interface ICommand
    {
        ///<summary> Execute function execute commands</summary>
        void Execute();
    }



}
