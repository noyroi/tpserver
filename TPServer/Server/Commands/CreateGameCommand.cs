﻿using System;
using Server.Commands;
using TPServer;

namespace Server
{
    class CreateGameCommand : Command
    {
        public CreateGameCommand()
        {
            paramNames.Add("courtId");
            paramNames.Add("creatorId");
            paramNames.Add("date");
            paramNames.Add("time");
        }
        public override Command Clone()
        {
            return new CreateGameCommand();
        }

        public override void Execute()
        {
            if (IsAllParamsExist())
            {
                DBConnector DB = new DBConnector();
                uint success;
                string numberOfPlayers = null, priv = null, duration = null, addedDate = DateTime.Today.Date.ToString("d");
                if (param.ContainsKey("playerNumber"))
                {
                    numberOfPlayers = param["playerNumber"];
                }
                if (param.ContainsKey("private"))
                {
                    priv = param["private"];
                }
                if (param.ContainsKey("duration"))
                {
                    duration = param["duration"];
                }
                success = DB.AddNewGameToDB(param["courtId"], param["creatorId"], param["date"],
                            param["time"], numberOfPlayers, priv, duration, addedDate);
                if (success == 0)
                    result = CreateAnswer("GU1", "err4", string.Empty);
                else
                    result = CreateAnswer("GU1", "success",success.ToString());
            }
            else
            {
                result = CreateMissingParamsAns("CU4");
            }
        }
    }
}