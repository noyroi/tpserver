﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPServer;

namespace Server.Commands
{
    class UpdateProfileCommand : Command
    {
        public UpdateProfileCommand()
        {
            paramNames.Add("uid");
        }

        public override Command Clone()
        {
            return new UpdateProfileCommand();
        }

        public override void Execute()
        {
            result = null;
            string val;
            uint uid;
            if (IsAllParamsExist())
            {
                DBConnector DB = new DBConnector();
                if (this.param.TryGetValue("uid", out val) &&
                    (this.param.TryGetValue("oldPassword", out val) &&
                    this.param.TryGetValue("newPassword", out val) &&
                    DB.CheckLoginDetails(Convert.ToUInt32(param["uid"]), param["oldPassword"]))
                    || this.param.TryGetValue("email", out val)
                    || this.param.TryGetValue("name", out val)
                    || this.param.TryGetValue("lastName", out val)
                    || this.param.TryGetValue("city", out val)
                    || this.param.TryGetValue("birthdate", out val))
                {

                    //TODO validate all parameters content
                    
                    //there is at least one parameter to change - can change
                    if (DB.UpdatePlayerDetails(this.param))
                        result = CreateAnswer("UP1", "success", string.Empty);
                    else
                        result = CreateAnswer("UP1", "err4", string.Empty);
                }
                else
                    result = CreateAnswer("UP1", "err5", string.Empty);
            }
            else
            {
                result = CreateMissingParamsAns("UP1");
            }
        }
    }
}
