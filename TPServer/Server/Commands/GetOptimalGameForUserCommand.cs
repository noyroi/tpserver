﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPServer;

namespace Server.Commands
{
    class GetOptimalGameForUserCommand : Command
    {
        public GetOptimalGameForUserCommand()
        {
            paramNames.Add("userId");
            paramNames.Add("LA");
            paramNames.Add("LO");
        }
        public override Command Clone()
        {
            return new GetOptimalGameForUserCommand();
        }

        public override void Execute()
        {
            if (IsAllParamsExist())
            {
                DBConnector DB = new DBConnector();
                string userId;
                this.param.TryGetValue("userId", out userId);
                List<Game> games = DB.GetGamesFromDB(userId);
                int optimalAge;
                double optimalLA, optimalLO;
                int optimalTime;
                List<Game> playersGames = new List<Game>();
                if (games.Count > 0)
                {
                    foreach (Game game in games)
                    {
                        playersGames.Add(DB.GetGameFromDB(game.uid.ToString()));
                    }
                    int numPlayers = 0;
                    int sumPlayers =0;
                    int sizeGames = playersGames.Count;
                    int timeSum = 0;
                    double LAsum = 0, LOsum = 0;
                    try {
                        foreach (Game game in playersGames)
                        {
                            foreach(Player player in game.players)
                            {
                                numPlayers++;
                                sumPlayers += player.Age;
                            }
                            Court court = DB.GetCourtById(game.CourtId.ToString());
                            LAsum += court.LA;
                            LOsum += court.LO;
                            timeSum += Int32.Parse(game.Time.Split(':')[0]);
                        }
                    }catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    if (numPlayers > 0)
                    {
                        optimalAge = sumPlayers / numPlayers;
                    } else
                    {
                        Player player = DB.GetPlayerFromDB(userId);
                        optimalAge = player.Age;
                    }
                    optimalLA = LAsum / sizeGames;
                    optimalLO = LOsum / sizeGames;
                    optimalTime = timeSum / sizeGames;

                } else
                {
                    Player player = DB.GetPlayerFromDB(userId);
                    optimalAge = player.Age;
                    String val;
                    this.param.TryGetValue("LA", out val);
                    optimalLA = double.Parse(val);
                    this.param.TryGetValue("LO", out val);
                    optimalLO = double.Parse(val);
                    optimalTime = 18;
                }
                List<Game> allGamestemp = DB.GetAllGamesFromDB();
                List<Game> allGames = new List<Game>();
                foreach (Game game in allGamestemp)
                {
                    allGames.Add(DB.GetGameFromDB(game.uid.ToString()));
                }
                Dictionary<Game, double> ranking = new Dictionary<Game, double>();
                try
                {
                    foreach (Game game in allGames)
                    {
                        double rank = 0;
                        Court court = DB.GetCourtById(game.CourtId.ToString());
                        rank += Math.Abs(optimalLA - court.LA);
                        rank += Math.Abs(optimalLO - court.LO);
                        rank += Math.Abs(Int32.Parse(game.Time.Split(':')[0]) - optimalTime);
                        Game temp = DB.GetGameFromDB(game.uid.ToString());
                        int num = 0;
                        int sum = 0;
                        foreach (Player player in temp.players)
                        {
                            num++;
                            sum += player.Age;
                        }
                        if (num > 0)
                            rank += Math.Abs((sum / num) - optimalAge);
                        ranking.Add(game, rank);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                Game optimalGame = ranking.Aggregate((l, r) => l.Value < r.Value ? l : r).Key;
                result = CreateAnswer("FL1", "success", optimalGame.uid.ToString());
            }
            else
            {
                result = CreateMissingParamsAns("FL1");
            }
        }
    }
}
