﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPServer;

namespace Server.Commands
{
    class AddCourtCommand : Command
    {
        public AddCourtCommand()
        {
            paramNames.Add("name");
            paramNames.Add("city");
        }
        public override Command Clone()
        {
            return new AddCourtCommand();
        }

        public override void Execute()
        {
            if (IsAllParamsExist())
            {
                DBConnector DB = new DBConnector();
                uint uid = DB.AddCourtToDB(this.param);
                if (uid == 0)
                    result = CreateAnswer("CU1", "err4",string.Empty);
                else
                    result = CreateAnswer("CU1", "success", uid.ToString());
            }
            else {
                result = CreateMissingParamsAns("CU1");
            }
        }
    }
}
