﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPServer;

namespace Server.Commands
{
    class AddUserToGameCommand : Command
    {
        public AddUserToGameCommand()
        {
            paramNames.Add("userId");
            paramNames.Add("gameId");
        }
    
        public override Command Clone()
        {
            return new AddUserToGameCommand();
        }

        public override void Execute()
        {
            result = null;
            string uid, gid;
            if (IsAllParamsExist())
            {
                this.param.TryGetValue("userId", out uid);
                this.param.TryGetValue("gameId", out gid);
                DBConnector DB = new DBConnector();
                Boolean ans = DB.addPlayerToGame(gid,uid);
                if (ans)
                {
                    result = CreateAnswer("AU1", "success", string.Empty);
                }
                //error in the DB server.
                else
                {
                    result = CreateAnswer("AU1", "err4", string.Empty);
                }
            }
            else
            {
                result = CreateMissingParamsAns("AU1");
            }
        }
    }
}
