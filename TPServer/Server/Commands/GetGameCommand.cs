﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPServer;

namespace Server.Commands
{
    class GetGameCommand : Command
    {
        public GetGameCommand()
        {
            paramNames.Add("uid");
        }
        public override Command Clone()
        {
            return new GetGameCommand();
        }

        public override void Execute()
        {
            result = null;
            string id;
            if (IsAllParamsExist())
            {
                this.param.TryGetValue("uid", out id);
                DBConnector DB = new DBConnector();
                Game game = DB.GetGameFromDB(id);
                //correct username and password - can delete user.
                if (game != null)
                {
                    JObject json = new JObject();
                    json.Add("creatorId", game.CreatorId);
                    json.Add("creator", game.Name);
                    json.Add("duration", game.Duration);
                    json.Add("time", game.Time);
                    json.Add("date", game.Date.ToString());
                    json.Add("numOfPlayers", game.NumOfPlayers);
                    json.Add("courtName", game.CourtName);
                    json.Add("private", game.Priv);
                    json.Add("courtAddress", game.CourtAddress);
                    json.Add("courtId", game.CourtId);
                    json.Add("addedDate", game.AddedDate);
                    JArray jarray = new JArray();
                    foreach (Player player in game.players)
                    {
                        JObject jplayer = new JObject();
                        jplayer.Add("uid", player.id);
                        jplayer.Add("username", player.Username);
                        jplayer.Add("age", player.Age);
                        jplayer.Add("email", player.Email);
                        jplayer.Add("firstName", player.FirstName);
                        jplayer.Add("lastName", player.LastName);
                        jplayer.Add("city", player.ResidentCity);
                        jplayer.Add("birthday", player.Birthdate.ToString());
                        jplayer.Add("picture", player.ProfilePicturePath);
                        jarray.Add(jplayer);
                    }
                    json.Add("players", jarray);
                    result = CreateAnswer("GG1", "success", json.ToString());
                }
                //error in the DB server.
                else
                {
                    result = CreateAnswer("GG1", "err4", string.Empty);
                }
            }
            else
            {
                result = CreateMissingParamsAns("GG1");
            }
           
        }
    }
}
