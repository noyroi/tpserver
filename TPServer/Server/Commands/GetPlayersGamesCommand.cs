﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPServer;
using Newtonsoft.Json.Linq;

namespace Server.Commands
{
    class GetPlayersGamesCommand : Command
    {

        public GetPlayersGamesCommand()
        {
            paramNames.Add("uid");
        }

        public override Command Clone()
        {
            return new GetPlayersGamesCommand();
        }

        public override void Execute()
        {
            result = null;
            string id;
            if (IsAllParamsExist())
            {
                this.param.TryGetValue("uid", out id);
                DBConnector DB = new DBConnector();
                List<Game> games = DB.GetGamesFromDB(id);
                //correct username and password - can delete user.
                if (games != null)
                {
                    JArray jarray = new JArray();
                    foreach (Game game in games)
                    {
                        JObject json = new JObject();
                        json.Add("id", game.uid);
                        json.Add("creator", game.Name);
                        json.Add("date", game.date);
                        json.Add("time", game.Time);
                        json.Add("numPlayers", game.NumOfPlayers);
                        json.Add("duration", game.Duration);
                        json.Add("private", game.Priv);
                        jarray.Add(json);
                    }
                    result = CreateAnswer("UG1", "success", jarray.ToString());
                }
                //error in the DB server.
                else
                {
                    result = CreateAnswer("UG1", "err4", string.Empty);
                }
            }
            else
            {
                result = CreateMissingParamsAns("UG1");
            }
        }
    }
}
