﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPServer;

namespace Server.Commands
{
    class AddCourtReviewCommand : Command
    {
        public AddCourtReviewCommand()
        {
            paramNames.Add("rank");
            paramNames.Add("courtId");
            paramNames.Add("rankerId");
        }

        public override Command Clone()
        {
            return new AddCourtReviewCommand();
        }

        public override void Execute()
        {
            if (IsAllParamsExist())
            {
                DBConnector DB = new DBConnector();
                bool success;
                if (param.ContainsKey("text"))
                {
                    success = DB.AddCourtReviewToDB(param["courtId"], param["rankerId"], param["rank"], param["text"]);
                } else
                {
                   success = DB.AddCourtReviewToDB(param["courtId"], param["rankerId"], param["rank"]);
                }
                if (!success)
                    result = CreateAnswer("CU4", "err4", string.Empty);
                else
                    result = CreateAnswer("CU4", "success", string.Empty);
            }
            else
            {
                result = CreateMissingParamsAns("CU4");
            }
        }
    }
}
