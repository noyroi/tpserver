﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPServer;

namespace Server.Commands
{
    class UpdateCourtCommand : Command
    {
        public UpdateCourtCommand()
        {
            paramNames.Add("uid");
        }
        public override Command Clone()
        {
            return new UpdateCourtCommand();
        }

        public override void Execute()
        {
            result = null;
            string val;
            if (IsAllParamsExist())
            {
                   DBConnector DB = new DBConnector();
                if (this.param.TryGetValue("uid", out val)
                    || this.param.TryGetValue("name", out val)
                    || this.param.TryGetValue("street", out val)
                    || this.param.TryGetValue("streetNum", out val)
                    || this.param.TryGetValue("kind", out val)
                    || this.param.TryGetValue("width", out val)
                    || this.param.TryGetValue("length", out val)
                    || this.param.TryGetValue("description", out val)
                    || this.param.TryGetValue("phone", out val))
                {
                    //there is at least one parameter to change - can change
                    if (DB.UpdateCourtDetails(this.param))
                        result = CreateAnswer("CU2", "success", string.Empty);
                    else
                        result = CreateAnswer("CU2", "err4", string.Empty);
                }
                else
                    result = CreateAnswer("CU2", "err5", string.Empty);
            }
            else
            {
                result = CreateMissingParamsAns("CU2");
            }
        }
    }
}
