﻿using System;
using Server.Commands;
using TPServer;

namespace Server
{
    class DeleteGameCommand : Command
    {
        public DeleteGameCommand()
        {
            paramNames.Add("gameId");
        }
        public override Command Clone()
        {
            return new DeleteGameCommand();
        }

        public override void Execute()
        {
            if (IsAllParamsExist())
            {
                DBConnector DB = new DBConnector();
                DB.DeleteGameFromDB(UInt32.Parse(param["gameId"]));
            }
            else
            {
                result = CreateMissingParamsAns("GU3");
            }
            result = CreateAnswer("GU3", "success", string.Empty);
        }
    }
}