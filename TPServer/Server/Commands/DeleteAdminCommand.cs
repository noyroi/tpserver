﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPServer;

namespace Server.Commands
{
    class DeleteAdminCommand : Command
    {
        public DeleteAdminCommand()
        {
            //uid to delete
            paramNames.Add("uid");
            //admin's details
            paramNames.Add("admin");
            paramNames.Add("password");
        }
        public override Command Clone()
        {
            return new DeleteAdminCommand();
        }

        public override void Execute()
        {
            result = null;
            string id, password, admin;
            if (IsAllParamsExist())
            {
                this.param.TryGetValue("uid", out id);
                this.param.TryGetValue("admin", out admin);
                this.param.TryGetValue("password", out password);

                //TODO validate all parameters content
                DBConnector DB = new DBConnector();
                uint uidAdmin = DB.IsAdminAuthorized(admin, password,1);
                //correct username and password (admin) - can delete user.
                if (uidAdmin > 0)
                {
                    if (DB.DeleteAdminFromDB(Convert.ToUInt32(id)))
                        result = CreateAnswer("UA2", "success", string.Empty);
                    else
                        result = CreateAnswer("UA2", "err4", string.Empty);
                }
                //incorrect username and password for admin login.
                else
                {
                    result = CreateAnswer("UA2", "err3", string.Empty);
                }
            }
            else
            {
                result = CreateMissingParamsAns("UA2");
            }
        }
    }
}
