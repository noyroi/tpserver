﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPServer;

namespace Server.Commands
{
    class GetPlayersDetailsCommand : Command
    {
        public GetPlayersDetailsCommand()
        {
            paramNames.Add("uid");
        }
        public override Command Clone()
        {
            return new GetPlayersDetailsCommand();
        }

        public override void Execute()
        {
            result = null;
            string id;
            if (IsAllParamsExist())
            {
                this.param.TryGetValue("uid", out id);
                DBConnector DB = new DBConnector();
                Player player = DB.GetPlayerFromDB(id);
                //correct username and password - can delete user.
                if (player != null)
                {   
                    JObject json = new JObject();
                    json.Add("username", player.Username);
                    json.Add("age", player.Age);
                    json.Add("email", player.Email);
                    json.Add("firstName", player.FirstName);
                    json.Add("lastName", player.LastName);
                    json.Add("city", player.ResidentCity);
                    json.Add("birthday", player.Birthdate.ToString());
                    json.Add("picture", player.ProfilePicturePath);
                    result = CreateAnswer("UP3", "success", json.ToString());
                }
                //error in the DB server.
                else
                {
                    result = CreateAnswer("UP3", "err4", string.Empty);
                }
            }
            else
            {
                result = CreateMissingParamsAns("UP3");
            }
        }
    }
}
