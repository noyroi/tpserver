﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using TPServer;
using System;

namespace Server.Commands
{
    public class SignUpCommand : Command
    {

        public SignUpCommand()
        {
            paramNames.Add("username");
            paramNames.Add("password");
            paramNames.Add("email");
            paramNames.Add("firstName");
            paramNames.Add("lastName");
            paramNames.Add("city");
            paramNames.Add("birthday");
        }

        public override Command Clone()
        {
            return new SignUpCommand();
        }

        public override void Execute()
        {
            result = null;
            string username, password, email, firstName, lastName, city, birthday;
            if (IsAllParamsExist())
            {
                this.param.TryGetValue("username", out username);
                this.param.TryGetValue("password", out password);
                this.param.TryGetValue("email", out email);
                this.param.TryGetValue("firstName", out firstName);
                this.param.TryGetValue("lastName", out lastName);
                this.param.TryGetValue("city", out city);
                this.param.TryGetValue("birthday", out birthday);
                //TODO validate all parameters content
                Player player = new Player(username, password, email, null, firstName, lastName, birthday, city);
                DBConnector DB = new DBConnector();
                //TODO check if user/email exist
                try {
                    string uid = DB.SavePlayerToDB(player).ToString();
                    result = CreateAnswer("UL1", "success", uid);
                }
                catch(Exception e)
                {
                    if (e.Message.Contains("Username"))
                        result = CreateAnswer("UL1", "err2","username");
                    else
                        result = CreateAnswer("UL1", "err2", "email");
                }
                
            }
            else {
                result = CreateMissingParamsAns("UL1");
            }
        }
    }
}