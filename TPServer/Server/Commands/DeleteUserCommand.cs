﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPServer;

namespace Server.Commands
{
    public class DeleteUserCommand : Command
    {
        public DeleteUserCommand()
        {
            paramNames.Add("uid");
            paramNames.Add("password");
        }
        public override Command Clone()
        {
            return new DeleteUserCommand();
        }

        public override void Execute()
        {
            result = null;
            string id, password, admin;
            if (IsAllParamsExist())
            {
                this.param.TryGetValue("uid", out id);
                this.param.TryGetValue("password", out password);
                this.param.TryGetValue("admin", out admin);

                //TODO validate all parameters content
                DBConnector DB = new DBConnector();

                bool validUser = false;
                uint uidAdmin = 0;
                if (admin != null)
                    uidAdmin = DB.CheckAdminLoginDetails(admin, password);
                else
                    validUser = DB.CheckLoginDetails(Convert.ToUInt32(id), password);

                //correct username and password - can delete user.
                if (validUser || uidAdmin > 0)
                {
                    if (DB.DeletePlayerFromDB(Convert.ToUInt32(id)))
                        result = CreateAnswer("UP2", "success", string.Empty);
                    else
                        result = CreateAnswer("UP2", "err4", string.Empty);
                }
                //incorrect username and password for user login and for admin login.
                else
                {
                    result = CreateAnswer("UP2", "err3", string.Empty);
                }
            }
            else
            {
                result = CreateMissingParamsAns("UP2");
            }
        }
    }
}
