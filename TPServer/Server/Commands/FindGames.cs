﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPServer;

namespace Server.Commands
{
    public class FindGames : Command
    {
        public override Command Clone()
        {
            return new FindGames();
        }

        public override void Execute()
        {
            result = null;
            if (IsAllParamsExist())
            {
                DBConnector DB = new DBConnector();
                List<Game> games = DB.FindGames(param);
                //correct username and password - can delete user.
                if (games != null)
                {
                    JArray jarray = new JArray();
                    foreach (Game game in games)
                    {
                        JObject json = new JObject();
                        json.Add("id", game.uid);
                        json.Add("creator", game.Name);
                        json.Add("date", game.date);
                        json.Add("time", game.Time);
                        json.Add("numPlayers", game.NumOfPlayers);
                        json.Add("duration", game.Duration);
                        json.Add("private", game.Priv);
                        jarray.Add(json);
                    }
                    result = CreateAnswer("SG1", "success", jarray.ToString());
                }
                //error in the DB server.
                else
                {
                    result = CreateAnswer("SG1", "err4", string.Empty);
                }
            }
            else
            {
                result = CreateMissingParamsAns("SG1");
            }
        }
    }
}
