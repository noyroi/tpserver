﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPServer;

namespace Server.Commands
{
    public class LoginCommand : Command
    {
        public LoginCommand()
        {
            paramNames.Add("username");
            paramNames.Add("password");
        }
        public override Command Clone()
        {
            return new LoginCommand();
        }

        public override void Execute()
        {
            result = null;
            string username, password;
            if (IsAllParamsExist())
            {
                this.param.TryGetValue("username", out username);
                this.param.TryGetValue("password", out password);
                //TODO validate all parameters content
                DBConnector DB = new DBConnector();
                uint uid = DB.CheckLoginDetails(username, password);
                if (uid > 0)
                {
                    result = CreateAnswer("UL2", "success", uid.ToString());
                }
                else
                {
                    result = CreateAnswer("UL2", "err3", string.Empty);
                }
            }
            else
            {
                result = CreateMissingParamsAns("UL2");
            }
        }
    }
}
