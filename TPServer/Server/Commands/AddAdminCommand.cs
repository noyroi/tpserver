﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TPServer;

namespace Server.Commands
{
    class AddAdminCommand : Command
    {
        public AddAdminCommand()
        {
            paramNames.Add("username");
            paramNames.Add("password");
            paramNames.Add("AUsername");
            paramNames.Add("APassword");
            paramNames.Add("Aemail");
            paramNames.Add("Apermission");
        }

        public override Command Clone()
        {
            return new AddAdminCommand();
        }

        public override void Execute()
        {
            if (IsAllParamsExist())
            { 
                DBConnector DB = new DBConnector();
                string username, password, AUsername, APassword, Aemail, Apermission;
                this.param.TryGetValue("username", out username);
                this.param.TryGetValue("password", out password);
                this.param.TryGetValue("AUsername", out AUsername);
                this.param.TryGetValue("APassword", out APassword);
                this.param.TryGetValue("Aemail", out Aemail);
                this.param.TryGetValue("Apermission", out Apermission);
                //check if the request to create new admin comes from an authorized admin
                uint uidAdmin = DB.IsAdminAuthorized(username, password,1);
                if (uidAdmin > 0)
                {
                    string newAdminUid = DB.SaveAdminToDB(AUsername, APassword, Aemail, Apermission).ToString();
                    result = CreateAnswer("UA1", "success", newAdminUid);
                }
            }
            else
            {
                result = CreateMissingParamsAns("UA1");
            }
        }
    }
}
