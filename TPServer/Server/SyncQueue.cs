﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Server.Commands;

namespace Server
{
    class SyncQueue
    {
        public Queue<Command> taskQueue { get; }
        private static volatile SyncQueue instance = null;
        private static readonly object padlock = new object();

        private SyncQueue()
        {
            taskQueue = new Queue<Command>();
        }


        public static SyncQueue Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new SyncQueue();
                    }
                    return instance;
                }
            }
        }
    }
}
