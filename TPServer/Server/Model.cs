﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Server.Commands;

namespace Server
{
    ///<summary> Model Class represents a logical side of the server</summary>
    public class Model
    {
        public Dictionary<string, Command> tasksDictionary { get; }
        private List<Task> myTasks = new List<Task>();

        public delegate void NewResultEventHandler(object source, Presenter dest, JObject result);
        public event NewResultEventHandler NewResult;

        ///<summary> Model Class represents a logical side of the server</summary>
        public Model()
        {
            tasksDictionary = new Dictionary<string, Command>()
            {
                {"UL1", new SignUpCommand() {}},
                {"UL2", new LoginCommand() {}},
                {"UP2", new DeleteUserCommand() {}},
                {"UP1", new UpdateProfileCommand() {}},
                {"CU1", new AddCourtCommand() {}},
                {"CU4", new AddCourtReviewCommand() {}},
                {"GU1", new CreateGameCommand() {}},
                {"GU3", new DeleteGameCommand() {}},
                {"GU4", new UpdateGameCommand() {}},
                {"UA1", new AddAdminCommand() {}},
                {"UA2", new DeleteAdminCommand() {}},
                {"CU2", new UpdateCourtCommand() {}},
                {"CU3", new DeleteCourtCommand() {}},
                {"UP3", new GetPlayersDetailsCommand() {}},
                {"UG1", new GetPlayersGamesCommand() {}},
                {"CA1", new GetAllCourtsCommand() {} },
                {"GG1", new GetGameCommand() {} },
                {"GC1", new GetAllCourtsGamesCommand() {} },
                {"AU1", new AddUserToGameCommand() {} },
                {"SG1", new FindGames() {} },
                {"FL1", new GetOptimalGameForUserCommand() {} }
            };
        }

        ///<summary> AddCommandToQueue function execute next command 
        ///by adding this command to list of commands</summary>
        public void AddCommandToQueue(Command command)
        {
            SyncQueue.Instance.taskQueue.Enqueue(command);
            myTasks.Add(Task.Factory.StartNew(this.Execute));
        }

        ///<summary> Execute function execute next command that in list</summary>
        private void Execute()
        {
            if (SyncQueue.Instance.taskQueue.Count > 0)
            {
                //get next command
                Command command = SyncQueue.Instance.taskQueue.Dequeue();
                command.Execute();
               //command.caller.ResultHandler(this, command.caller, command.result);
                if (NewResult != null)
                {
                    NewResult(this, command.caller, command.result);
                }
            }
        }
    }
}
