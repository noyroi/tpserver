﻿using System.Net.Sockets;
using System.Text;


namespace Server
{
    ///<summary> View Class is in charge of all logic of the server
    ///that handles sending and receiving messages from client</summary>
    public class View
    {
        private Socket client;
        public delegate void NewInputEventHandler(object source, string input);
        public event NewInputEventHandler NewInput;

        ///<summary>constructor</summary>
        public View(Socket client)
        {
            this.client = client;
        }

        ///<summary>Start communicaction between the server and the client - receive message</summary>
        public void Start()
        {
            while (true)
            {
                byte[] data = new byte[8192*2];
                int recv = client.Receive(data);
                if (recv == 0)
                    break;
                string str = Encoding.UTF8.GetString(data, 0, recv);
                if (NewInput != null)
                {
                    NewInput(this, str);
                }
            }
        }

        ///<summary>communicaction between the server and the client - send message</summary>
        public void Send(string result)
        {
            result = Encoding.UTF8.GetBytes(result).Length + result;
            System.Console.WriteLine(result);
            System.Console.WriteLine("sent: " + Encoding.UTF8.GetBytes(result).Length);
            client.Send(Encoding.UTF8.GetBytes(result), Encoding.UTF8.GetBytes(result).Length, SocketFlags.None);
        }
    }
}
