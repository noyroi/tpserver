﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Server.Commands;

namespace Server
{
    ///<summary> Presenter Class represents the connector between
    ///view and the logical part of the server</summary>
    public class Presenter
    {
        private View clientHandler;
        private Model model;
        private Dictionary<string, Command> tasksDictionary;

        ///<summary>constructor</summary>
        public Presenter(Model model, View clientHandler)
        {
            this.tasksDictionary = model.tasksDictionary;
            this.model = model;
            this.clientHandler = clientHandler;
        }

        ///<summary>InputHandler function in charge of operating
        ///commands in the model</summary>
        internal void InputHandler(object source, string input)
        {

            //get json from string
            JObject req = JObject.Parse(input);
            string task = req["command"].Value<string>();
            JObject parms = req["parms"].Value<JObject>();
            Dictionary<string, string> tokens = JsonConvert.DeserializeObject<Dictionary<string, string>>(parms.ToString());
            Command command;
            if (!tasksDictionary.TryGetValue(task, out command))
            {
                Console.WriteLine("404 option not found:" + input);
            }
            else
            {
                Console.WriteLine(input);
                Command newCommand = command.Clone();
                newCommand.param = tokens;
                newCommand.caller = this;
                model.AddCommandToQueue(newCommand);
            }
        }

        ///<summary>ResultHandler function in charge of sending back to the view the result
        ///after the model operation</summary>
        public void ResultHandler(object source, Presenter dest, JObject result)
        {
            //null will be sent when no message is necassary  (1 multiplayer or close command)
            if (result != null)
            {
                if (dest == this)
                {
                    clientHandler.Send(result.ToString() + '\n');
                }
            }
        }
    }

}
