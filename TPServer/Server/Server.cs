﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace Server
{
    ///<summary> Server Class that handles server's logic</summary>
    public class Server
    {
        private int port;
        private IPEndPoint ipep;
        private Socket newsock;
        //private List<Task> myTasks = new List<Task>();

        ///<summary>constructor</summary>
        public Server(int port)
        {
            this.port = port;
        }

        ///<summary>Start function begins with server's logic</summary>
        public void Start()

        {
            Model model = new Model();
            //opening a socket
            ipep = new IPEndPoint(IPAddress.Any, port);
            newsock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            newsock.Bind(ipep);
            newsock.Listen(10);

            while (true)
            {
                Socket client = newsock.Accept();
                View clientHandler = new View(client);
                Presenter presenter = new Presenter(model, clientHandler);
                clientHandler.NewInput += presenter.InputHandler;
                model.NewResult += presenter.ResultHandler;
                Thread t = new Thread(clientHandler.Start);
                t.Start();
                //myTasks.Add(Task.Factory.StartNew(clientHandler.Start));
            }
        }
    }
}
