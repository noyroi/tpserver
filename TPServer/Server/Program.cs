﻿using System;
using System.Configuration;
using TPServer;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            DBConnector DB = new DBConnector();
            //DB.InitializeDB();
            int port = Int32.Parse(ConfigurationManager.AppSettings["serverPort"]);
            Server s = new Server(port);
            s.Start();
        }
    }
}
