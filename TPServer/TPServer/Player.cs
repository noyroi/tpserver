﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace TPServer
{
    public class Player : User
    {
        #region members

        //Assuming that the picture is already on the server
        public string ProfilePicturePath
        {
            get;
        }

        public string FirstName
        {
            get;
        }

        public string LastName
        {
            get;
        }

        public string ResidentCity
        {
            get;
        }

        public string birthdate;
        public int id
        {
            get;
        }

        public DateTime Birthdate {
            get
            {
                String[] splitdate = birthdate.Split('/');
                int day = Int32.Parse(splitdate[0]), month = Int32.Parse(splitdate[1]), year = Int32.Parse(splitdate[2]);
                return new DateTime(year, month, day);
            }
        }

        public int Age
        {
            get
            {
                // Save today's date.
                var today = DateTime.Today;
                // Calculate the age.
                var age = today.Year - Birthdate.Year;
                if (Birthdate > today.AddYears(-age)) age--;
                return age;
            }
        }

        public Rank MyRanking
        {
            get;
        }
        #endregion

        #region methods
        public Player() :base()
        {

        }
        public Player(string username, string password, string email, string ProfilePicturePath,
                        string first, string last, string birthdate, string city) : base(username, password, email)
        {
            FirstName = first;
            LastName = last;
            ResidentCity = city;
            MyRanking = new Rank();
            this.birthdate = birthdate;
            this.ProfilePicturePath = "";
        }
        public Player(int id, string username, string password, string email, string ProfilePicturePath,
                string first, string last, string birthdate, string city) : base(username, password, email)
        {
            FirstName = first;
            LastName = last;
            ResidentCity = city;
            MyRanking = new Rank();
            this.birthdate = birthdate;
            this.ProfilePicturePath = "";
            this.id = id;
        }
        #endregion
    }
}
