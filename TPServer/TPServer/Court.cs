﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPServer
{
    public class Court
    {
        public Court (uint id, string name, string city, string street, uint streetNum, string kind,uint width, uint length, double LA, double LO, string des)
        {
            this.description = des;
            this.Id = id;
            this.Name = name;
            this.City = city;
            this.Street = street;
            this.StreetNumber = streetNum;
            this.kind = kind;
            this.width = width;
            this.length = length;
            this.LA = LA;
            this.LO = LO;
        }
        public string Name
        {
            get;
        }
        public string City
        {
            get;
        }
        public string Street
        {
            get;
        }
        public uint StreetNumber
        {
            get;
        }
        public string kind
        {
            get;
        }
        public uint width
        {
            get;
        }
        public uint length
        {
            get;
        }
        public string description { get; }
        public uint Id { get; }
        public double LA { get; }
        public double LO { get; }
    }
}
