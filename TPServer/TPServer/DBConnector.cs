﻿using System;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Collections.Generic;

namespace TPServer
{
    public class DBConnector
    {
        #region members
        private MySqlConnection connection;

        #endregion

        #region connection methods
        public DBConnector()
        {
            string server = ConfigurationManager.AppSettings["dbServer"];
            string db = ConfigurationManager.AppSettings["database"];
            string user = ConfigurationManager.AppSettings["dbUser"];
            string pas = ConfigurationManager.AppSettings["dbPassword"];
            string connectionString = "SERVER=" + server + ";" + "DATABASE=" + db + ";" +
                               "UID=" + user + ";" + "PASSWORD=" + pas + ";";
            this.connection = new MySqlConnection(connectionString);
        }

        public Player GetPlayerFromDB(string id)
        {
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "SELECT Username,LastName,FirstName,City,Email," +
                "Birthday FROM players WHERE ID = ?Id;";
            
            comm.Parameters.AddWithValue("?Id", Int32.Parse(id));

            MySqlDataReader result;
            if (OpenConnection())
            {
                try
                {
                    result = comm.ExecuteReader();
                }
                catch (Exception e)
                {
                    result = null;
                    //TODO query exception
                }
            }
            else
            {
                return null;
            }

            //checks if the username exists
            if (result != null && result.Read()) {
                string username ="";
                string email = "";
                string firstname="";
                string lastname="";
                string birthdate="";
                string city="";
                try
                {
                    username = (string)result["Username"];
                    email = (string)result["Email"];
                    firstname = (string)result["FirstName"];
                    lastname = (string)result["LastName"];
                    birthdate = (string)result["Birthday"];
                    city = (string)result["City"];
                } catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                Player p = new Player(username, "0",email , "",
                        firstname, lastname, 
                        birthdate, city);
                CloseConnection();
                return p;
            }
            else
            {
                CloseConnection();
                return null;
            }

        }

        public bool DeleteGameFromDB(uint gameId)
        {
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "DELETE FROM games WHERE ID = ?gameId";
            comm.Parameters.AddWithValue("?gameId", gameId);
            if (OpenConnection())
            {
                try
                {
                    comm.ExecuteNonQuery();
                    CloseConnection();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    return false;
                }
            }
            return true;
        }

        public bool AddCourtReviewToDB(string courtId, string rankerId, string rank, string text = null)
        {
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "INSERT INTO CourtRankingsLog (IDCourt, rankerId, rank, text) " +
                                "VALUES (?courtId, ?rankerId, ?rank, ?text);";
            comm.Parameters.AddWithValue("?courtId", courtId);
            comm.Parameters.AddWithValue("?rankerId", rankerId);
            comm.Parameters.AddWithValue("?rank", rank);
            comm.Parameters.AddWithValue("?text", text);
            if (OpenConnection())
            {
                try
                {
                    comm.ExecuteNonQuery();
                    CloseConnection();
                } catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    return false;
                }
            }
            else
            {
                Console.WriteLine("error writing to db");
                return false;
            }
            return true;
        }

        public uint AddNewGameToDB(string courtId, string creatorId, string date, string time, string numberOfPlayers, string priv, string duration, string addedDate)
        {
            uint uid = 0;
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "INSERT INTO games (creator, date, time, addedDate";
            string values= ") VALUES (?creatorId, ?date, ?time, ?addedDate";
            comm.Parameters.AddWithValue("?creatorId", creatorId);
            comm.Parameters.AddWithValue("?date", date);
            comm.Parameters.AddWithValue("?time", time);
            comm.Parameters.AddWithValue("?addedDate", addedDate);
            if (numberOfPlayers != null)
            {
                comm.CommandText += ", numberOfPlayers";
                values += ", ?numberOfPlayers";
                comm.Parameters.AddWithValue("?numberOfPlayers", numberOfPlayers);
            }
            if (priv != null)
            {
                comm.CommandText += ", private";
                values += ", ?private";
                if (priv.ToLower().Equals("true"))
                {
                    comm.Parameters.AddWithValue("?private", true);
                }
                else
                {
                    comm.Parameters.AddWithValue("?private", false);
                }
                
            }
            if (duration != null)
            {
                comm.CommandText += ", duration";
                values += ", ?duration";
                comm.Parameters.AddWithValue("?duration", duration);
            }
            comm.CommandText += values + "); INSERT INTO gameCourt (IDGame, IDcourt) VALUES (LAST_INSERT_ID(), ?courtId);"+
                "INSERT INTO gameplayer (IDGame, IDPlayer) values (LAST_INSERT_ID(),?creatorId);" +
                "SELECT ID FROM GAMES WHERE ID = LAST_INSERT_ID();";
            comm.Parameters.AddWithValue("?courtId", courtId);
            if (OpenConnection())
            {
                try
                {
                    uid = Convert.ToUInt32(comm.ExecuteScalar());
                    CloseConnection();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    return uid;
                }
            }
            else
            {
                Console.WriteLine("error writing to db");
                return uid;
            }
            return uid;
        }

        private bool OpenConnection()
        {
            try
            {
                connection.Open();
            }
            catch (Exception ex)
            {
                if (ex.Message.ToLower().Contains("already open")) { 
                    CloseConnection();
                    OpenConnection();
                }
                else
                    return false;
            }
            return true;
        }

        private bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }
        #endregion

        #region admins
        public uint CheckAdminLoginDetails(string username, string password)
        {
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "SELECT ID, UPassword FROM admins WHERE Username = ?Username";
            comm.Parameters.AddWithValue("?Username", username);
            if (OpenConnection())
            {
                MySqlDataReader result;
                try
                {
                    result = comm.ExecuteReader();
                }
                catch (Exception e)
                {
                    result = null;
                    //TODO query exception
                }

                //checks if the username exists
                if (result != null && result.Read())
                {
                    string resultPassword = (string)result["UPassword"];
                    uint uid = Convert.ToUInt32(result["ID"]);
                    CloseConnection();
                    if (resultPassword.Equals(password))
                    {
                        return uid;
                    }
                }
                else
                {
                    CloseConnection();
                }
            }
            else
            {
                //TODO send DB error
                Console.WriteLine("error opening connection");
            }
            return 0;
        }

        public uint IsAdminAuthorized(string username, string password,int per)

        {
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "SELECT ID, UPassword,permissionLevel FROM admins WHERE Username = ?Username";
            comm.Parameters.AddWithValue("?Username", username);
            if (OpenConnection())
            {
                MySqlDataReader result;
                try
                {
                    result = comm.ExecuteReader();
                }
                catch (Exception e)
                {
                    result = null;
                    //TODO query exception
                }

                //checks if the username exists
                if (result != null && result.Read())
                {
                    string resultPassword = (string)result["UPassword"];
                    uint uid = Convert.ToUInt32(result["ID"]);
                    int permission = Convert.ToInt32(result["permissionLevel"]);
                    CloseConnection();
                    // permission level = 1 means that this is a super-admin
                    //if entered permission level = 3 - admin who has permission
                    //level that is lower than 3 can access.
                    if (resultPassword.Equals(password) && permission <= per)
                    {
                        return uid;
                    }
                }
                else
                {
                    CloseConnection();
                }
            }
            else
            {
                //TODO send DB error
                Console.WriteLine("error opening connection");
            }
            return 0;
        }

        /// <summary>
        /// will save the new admin details to the DB
        /// </summary>
        public uint SaveAdminToDB(string username, string password, string email, string permission)
        {
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "INSERT INTO"
                + " admins(Username,UPassword,Email,permissionLevel,SignUpDate)"
                + " VALUES(?Username,?UPassword,?Email,?permissionLevel,?SignUpDate);"
                + "select ID FROM admins WHERE Username = ?Username;";
            comm.Parameters.AddWithValue("?Username", username);
            comm.Parameters.AddWithValue("?UPassword", password);
            comm.Parameters.AddWithValue("?Email", email);
            comm.Parameters.AddWithValue("?permissionLevel", permission);
            comm.Parameters.AddWithValue("?SignUpDate", DateTime.Today.Date.ToString("d"));
            uint newID = 0;
            if (OpenConnection())
            {
                newID = Convert.ToUInt32(comm.ExecuteScalar());
                CloseConnection();
            }
            else {
                Console.WriteLine("error writing to db");
            }
            return newID;
        }
        
        /// <summary>
        /// will delete the admins details from the DB
        /// </summary>
        public bool DeleteAdminFromDB(uint uid)
        {
            MySqlCommand comm = connection.CreateCommand();

            comm.CommandText = "DELETE FROM admins WHERE ID = ?UID";
            comm.Parameters.AddWithValue("?UID", uid);
            if (OpenConnection())
            {
                try
                {
                    comm.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    CloseConnection();
                    return false;
                }
                CloseConnection();
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region players
        /// <summary>
        /// will save the new player details to the DB
        /// </summary>
        public uint SavePlayerToDB (Player newPlayer)
        {
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "INSERT INTO"
                + " players(Username,UPassword,Email,FirstName,LastName,City,Birthday,SignUpDate)"
                + " VALUES(?Username,?UPassword,?Email,?FirstName,?LastName,?City,?Birthday, ?SignUpDate);"
                + "select ID FROM players WHERE Username = ?Username;";
            comm.Parameters.AddWithValue("?Username", newPlayer.Username);
            comm.Parameters.AddWithValue("?UPassword", newPlayer.Password);
            comm.Parameters.AddWithValue("?Email", newPlayer.Email);
            comm.Parameters.AddWithValue("?FirstName", newPlayer.FirstName);
            comm.Parameters.AddWithValue("?LastName", newPlayer.LastName);
            comm.Parameters.AddWithValue("?City", newPlayer.ResidentCity);
            comm.Parameters.AddWithValue("?Birthday", newPlayer.Birthdate.Date.ToString("d"));
            comm.Parameters.AddWithValue("?SignUpDate", DateTime.Today.Date.ToString("d"));
            uint newID = 0;
            if (OpenConnection())
            {
                try
                {
                    newID = Convert.ToUInt32(comm.ExecuteScalar());
                } catch (Exception e)
                {
                    throw e;
                }
                
                CloseConnection();
            }
            else {
                Console.WriteLine("error writing to db");
            }
            return newID;
        }

        public bool UpdatePlayerDetails(Dictionary<string, string> parms)
        {
            string uidString;
            parms.TryGetValue("uid", out uidString);
            uint uid = UInt32.Parse(uidString);
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "INSERT INTO playersHistory (SELECT *,?TodayDate,'U' FROM players WHERE ID = ?UID); UPDATE players SET ";
            string val = string.Empty;
            if(parms.TryGetValue("newPassword",out val)){
                comm.CommandText += "UPassword= ?UPassword,";
                comm.Parameters.AddWithValue("?UPassword", val);
            }
            if (parms.TryGetValue("email", out val))
            {
                comm.CommandText += "Email= ?Email,";
                comm.Parameters.AddWithValue("?Email", val);
            }
            if (parms.TryGetValue("name", out val))
            {
                comm.CommandText += "FirstName= ?FirstName,";
                comm.Parameters.AddWithValue("?FirstName", val);
            }
            if (parms.TryGetValue("lastName", out val))
            {
                comm.CommandText += "LastName= ?LastName,";
                comm.Parameters.AddWithValue("?LastName", val);
            }
            if (parms.TryGetValue("birthdate", out val))
            {
                comm.CommandText += "Birthday= ?Birthday,";
                comm.Parameters.AddWithValue("?Birthday", val);
            }
            if (parms.TryGetValue("city", out val))
            {
                comm.CommandText += "City= ?City,";
                comm.Parameters.AddWithValue("?City", val);
            }
            comm.CommandText = comm.CommandText.TrimEnd(',');
            comm.CommandText += " WHERE ID = ?UID";
            comm.Parameters.AddWithValue("?UID",uid);
            comm.Parameters.AddWithValue("?TodayDate", DateTime.Today.Date.ToString("d"));
            if (OpenConnection())
            {
                try
                {
                    comm.ExecuteScalar();
                } catch (Exception e)
                {
                    //TODO query error
                    CloseConnection();
                    return false;
                }
                CloseConnection();
                return true;

            }
            else
            {
                Console.WriteLine("error writing to db");
            }
            return false;
        }

        /// <summary>
        /// will delete the player details from the DB
        /// </summary>
        public bool DeletePlayerFromDB(uint uid)
        {
            //TODO add history table and store the player's details there
            MySqlCommand comm = connection.CreateCommand();
            
            comm.CommandText = "INSERT INTO playersHistory (SELECT *,?TodayDate,'D' FROM players WHERE ID = ?UID); DELETE FROM players WHERE ID = ?UID";
            comm.Parameters.AddWithValue("?UID", uid);
            comm.Parameters.AddWithValue("?TodayDate", DateTime.Today.Date.ToString("d"));
            if (OpenConnection())
            {
                try
                {
                    comm.ExecuteNonQuery();
                } catch (Exception e)
                {
                    CloseConnection();
                    return false;
                }
                CloseConnection();
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// will verify if the given password matches the user password
        /// </summary>
        /// <returns>UID if the password matches, 0 otherwise</returns>
        public uint CheckLoginDetails(string username, string password)
        {
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "SELECT ID, UPassword FROM players WHERE Username = ?Username";
            comm.Parameters.AddWithValue("?Username", username);
            if (OpenConnection())
            {
                MySqlDataReader result;
                try
                {
                    result = comm.ExecuteReader();
                } catch (Exception e)
                {
                    result = null;
                    //TODO query exception
                }
                
                //checks if the username exists
                if (result != null && result.Read())
                {
                    string resultPassword = (string)result["UPassword"];
                    uint uid = Convert.ToUInt32(result["ID"]);
                    CloseConnection();
                    if (resultPassword.Equals(password))
                    {
                        return uid;
                    }
                } else
                {
                    CloseConnection();
                }
            }
            else
            {
                //TODO send DB error
                Console.WriteLine("error opening connection");
            }
            return 0;
        }

        public bool CheckLoginDetails(uint uid, string password)
        {
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "SELECT UPassword FROM players WHERE ID = ?Uid";
            comm.Parameters.AddWithValue("?Uid", uid);
            if (OpenConnection())
            {
                MySqlDataReader result;
                try
                {
                    result = comm.ExecuteReader();
                }
                catch (Exception e)
                {
                    result = null;
                    //TODO query exception
                }

                //checks if the username exists
                if (result != null && result.Read())
                {
                    string resultPassword = (string)result["UPassword"];
                    CloseConnection();
                    if (resultPassword.Equals(password))
                    {
                        return true;
                    }
                }
                else
                {
                    CloseConnection();
                }
            }
            else
            {
                //TODO send DB error
                Console.WriteLine("error opening connection");
            }
            return false;
        }
        #endregion players

        #region court

        public Court GetCourtById(string id)
        {
            Court court = null ;
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "SELECT * FROM courts WHERE ID=?Id;";
            comm.Parameters.AddWithValue("?Id", id);
            MySqlDataReader result;
            if (OpenConnection())
            {
                try
                {
                    result = comm.ExecuteReader();
                }
                catch (Exception e)
                {
                    result = null;
                    //TODO query exception
                }
            }
            else
            {
                return null;
            }

            //checks if there are games
            if (result != null && result.Read())
            {
                int uid = 0;
                int width = 0;
                int length = 0;
                string name = "";
                string city = "";
                string street = "";
                string kind = "";
                int streetNum = 0;
                double LA = 0;
                double LO = 0;
                string description = "";
                try
                {
                    uid = (int)result["ID"];
                    width = (int)result["width"];
                    length = (int)result["length"];
                    name = (string)result["name"];
                    city = (string)result["city"];
                    street = (string)result["street"];
                    streetNum = (int)result["streetNum"];
                    kind = (string)result["kind"];
                    LA = (double)result["LA"];
                    LO = (double)result["LO"];
                    if (result["description"] != DBNull.Value)
                        description = (string)result["description"];
                    else
                        description = "";
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                court = new Court((uint)uid, name, city, street, (uint)streetNum, kind, (uint)width, (uint)length, LA, LO, description);
                CloseConnection();
            }
            else if (result == null)
            {
                return null;
            }
            return court;
        }

        public List<Court> GetAllCourtsFromDB()
        {
            List<Court> courts = new List<Court>();
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "SELECT * FROM courts;";

            MySqlDataReader result;
            if (OpenConnection())
            {
                try
                {
                    result = comm.ExecuteReader();
                }
                catch (Exception e)
                {
                    result = null;
                    //TODO query exception
                }
            }
            else
            {
                return null;
            }

            //checks if there are games
            if (result != null && result.Read())
            {
                int uid = 0;
                int width = 0;
                int length = 0;
                string name = "";
                string city = "";
                string street = "";
                string kind = "";
                int streetNum = 0;
                double LA = 0;
                double LO = 0;
                string description = "";

                do
                {
                    try
                    {
                        uid = (int)result["ID"];
                        width = (int)result["width"];
                        length = (int)result["length"];
                        name = (string)result["name"];
                        city = (string)result["city"];
                        street = (string)result["street"];
                        streetNum = (int)result["streetNum"];
                        kind = (string)result["kind"];
                        LA = (double)result["LA"];
                        LO = (double)result["LO"];
                        if (result["description"] != DBNull.Value)
                            description = (string)result["description"];
                        else
                            description = "";
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    courts.Add(new Court((uint)uid,name,city,street,(uint)streetNum,kind,(uint)width, (uint)length, LA, LO, description));
                } while (result.Read());
                CloseConnection();
            }
            else if (result == null)
            {
                return null;
            }
            return courts;
        }
        public uint AddCourtToDB(Dictionary<string, string> parms)
        {
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "INSERT INTO courts (name, city";
            string values = "VALUES (?name,?city";
            string val = string.Empty;
            parms.TryGetValue("name", out val);
            comm.Parameters.AddWithValue("?name", val);
            parms.TryGetValue("city", out val);
            comm.Parameters.AddWithValue("?city", val);
            if (parms.TryGetValue("street", out val))
            {
                comm.CommandText += ",street";
                values += ",?street";
                comm.Parameters.AddWithValue("?street", val);
            }
            if (parms.TryGetValue("streetNum", out val))
            {
                comm.CommandText += ",streetNum";
                values += ",?streetNum";
                comm.Parameters.AddWithValue("?streetNum", val);
            }
            if (parms.TryGetValue("kind", out val))
            {
                comm.CommandText += ",kind";
                values += ",?kind";
                comm.Parameters.AddWithValue("?kind", val);
            }
            if (parms.TryGetValue("width", out val))
            {
                comm.CommandText += ",width";
                values += ",?width";
                comm.Parameters.AddWithValue("?width", val);
            }
            if (parms.TryGetValue("length", out val))
            {
                comm.CommandText += ",length";
                values += ",?length";
                comm.Parameters.AddWithValue("?length", val);
            }
            if (parms.TryGetValue("description", out val))
            {
                comm.CommandText += ",description";
                values += ",?description";
                comm.Parameters.AddWithValue("?description", val);
            }
            if (parms.TryGetValue("phone", out val))
            {
                comm.CommandText += ",phone";
                values += ",?phone";
                comm.Parameters.AddWithValue("?phone", val);
            }
            values += ",?addedDate); select ID FROM courts WHERE name = ?name;";
            comm.CommandText += ",addedDate) " + values;
            comm.Parameters.AddWithValue("?addedDate", DateTime.Today.Date.ToString("d"));
            uint newID = 0;
            if (OpenConnection())
            {
                try
                {
                    newID = Convert.ToUInt32(comm.ExecuteScalar());
                    CloseConnection();
                } catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            else
            {
                Console.WriteLine("error writing to db");
            }
            return newID;
        }

        public bool UpdateCourtDetails(Dictionary<string, string> parms)
        {
            string uidString;
            parms.TryGetValue("uid", out uidString);
            uint uid = UInt32.Parse(uidString);
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "UPDATE courts SET ";
            string val = string.Empty;
            if (parms.TryGetValue("name", out val))
            {
                comm.CommandText += "name= ?name,";
                comm.Parameters.AddWithValue("?name", val);
            }
            if (parms.TryGetValue("street", out val))
            {
                comm.CommandText += "street= ?street,";
                comm.Parameters.AddWithValue("?street", val);
            }
            if (parms.TryGetValue("streetNum", out val))
            {
                comm.CommandText += "streetNum= ?streetNum,";
                comm.Parameters.AddWithValue("?streetNum", val);
            }
            if (parms.TryGetValue("kind", out val))
            {
                comm.CommandText += "kind= ?kind,";
                comm.Parameters.AddWithValue("?kind", val);
            }
            if (parms.TryGetValue("width", out val))
            {
                comm.CommandText += "width= ?width,";
                comm.Parameters.AddWithValue("?width", val);
            }
            if (parms.TryGetValue("length", out val))
            {
                comm.CommandText += "length= ?length,";
                comm.Parameters.AddWithValue("?length", val);
            }
            if (parms.TryGetValue("description", out val))
            {
                comm.CommandText += "description= ?description,";
                comm.Parameters.AddWithValue("?description", val);
            }
            if (parms.TryGetValue("phone", out val))
            {
                comm.CommandText += "phone= ?phone,";
                comm.Parameters.AddWithValue("?phone", val);
            }
            comm.CommandText = comm.CommandText.TrimEnd(',');
            comm.CommandText += " WHERE ID = ?UID";
            comm.Parameters.AddWithValue("?UID", uid);
            if (OpenConnection())
            {
                try
                {
                    comm.ExecuteScalar();
                }
                catch (Exception e)
                {
                    //TODO query error
                    CloseConnection();
                    return false;
                }
                CloseConnection();
                return true;

            }
            else
            {
                Console.WriteLine("error writing to db");
            }
            return false;
        }

        /// <summary>
        /// will delete the court details from the DB
        /// </summary>
        public bool DeleteCourtFromDB(uint uid)
        {
            MySqlCommand comm = connection.CreateCommand();

            comm.CommandText = "DELETE FROM courts WHERE ID = ?UID";
            comm.Parameters.AddWithValue("?UID", uid);
            if (OpenConnection())
            {
                try
                {
                    comm.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    CloseConnection();
                    return false;
                }
                CloseConnection();
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region initialization
        private void CreatePlayersTable()
        {
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "CREATE TABLE players" + 
                "(ID int NOT NULL auto_increment primary KEY, Username varchar(255) NOT NULL UNIQUE," +
                "UPassword varchar(255) NOT NULL, Email varchar(255) NOT NULL UNIQUE," +
                "FirstName varchar(255) NOT NULL, LastName varchar(255) NOT NULL," +
                "City varchar(255) NOT NULL, Birthday varchar(10) NOT NULL," +
                "SignUpDate varchar(10) NOT NULL)";
            if (OpenConnection())
            {
                try
                {
                    comm.ExecuteNonQuery();
                    
                } catch (Exception e)
                {
                    //TODO query error
                }
                CloseConnection();
            } else
            {
                Console.WriteLine("error opening connection");
            }

        }
        private void CreatePlayersHistoryTable()
        {
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "CREATE TABLE playersHistory" +
                "(ID int NOT NULL, Username varchar(255) NOT NULL," +
                "UPassword varchar(255) NOT NULL, Email varchar(255) NOT NULL," +
                "FirstName varchar(255) NOT NULL, LastName varchar(255) NOT NULL," +
                "City varchar(255) NOT NULL, Birthday varchar(10) NOT NULL," +
                "SignUpDate varchar(10) NOT NULL, UpdateDate varchar(10) NOT NULL, Status varchar(1) NOT NULL)";
            if (OpenConnection())
            {
                try
                {
                    comm.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    //TODO query error
                }
                CloseConnection();
            }
            else
            {
                Console.WriteLine("error opening connection");
            }
        }
        private void CreateAdminsTable()
        {
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "CREATE TABLE admins" +
                "(ID int NOT NULL auto_increment primary KEY, Username varchar(255) NOT NULL UNIQUE," +
                "UPassword varchar(255) NOT NULL, Email varchar(255) NOT NULL UNIQUE," +
                "permissionLevel int NOT NULL, SignUpDate varchar(10) NOT NULL)";
            if (OpenConnection())
            {
                try
                {
                    comm.ExecuteNonQuery();

                }
                catch (Exception e)
                {
                    //TODO query error
                }
                CloseConnection();
            }
            else
            {
                Console.WriteLine("error opening connection");
            }
        }

        private void CreateCourtsTable()
        {
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "CREATE TABLE courts" +
                "(ID int NOT NULL auto_increment primary KEY, name varchar(255) NOT NULL UNIQUE," +
                "city varchar(255) NOT NULL, street varchar(255), streetNum int, kind varchar(255)," +
                "width int,length int,description varchar(511),phone varchar(10), addedDate varchar(10) NOT NULL, LA Double NOT NULL, LO Double NOT NULL)";
            if (OpenConnection())
            {
                try
                {
                    comm.ExecuteNonQuery();

                }
                catch (Exception e)
                {
                    //TODO query error
                }
                CloseConnection();
            }
            else
            {
                Console.WriteLine("error opening connection");
            }
        }

        private void CreateGamesTable()
        {
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "CREATE TABLE games" +
                "(ID int NOT NULL auto_increment primary KEY," +
                "creator int NOT NULL, date varchar(10) NOT NULL, time varchar(5) NOT NULL," +
                "numberOfPlayers int DEFAULT 22 NOT NULL, private BOOLEAN DEFAULT FALSE NOT NULL,duration int DEFAULT 90 NOT NULL," +
                "addedDate varchar(10) NOT NULL)";
            if (OpenConnection())
            {
                try
                {
                    comm.ExecuteNonQuery();

                }
                catch (Exception e)
                {
                    //TODO query error
                }
                CloseConnection();
            }
            else
            {
                Console.WriteLine("error opening connection");
            }
        }

        private void CreateGameCourtTable()
        {
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "CREATE TABLE gameCourt" +
                "(IDGame int NOT NULL primary KEY UNIQUE," +
                "IDcourt int NOT NULL)";
            if (OpenConnection())
            {
                try
                {
                    comm.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    //TODO query error
                }
                CloseConnection();
            }
            else
            {
                Console.WriteLine("error opening connection");
            }
        }

        private void CreateGamePlayerTable()
        {
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "CREATE TABLE gamePlayer" +
                "(IDGame int NOT NULL,"+
                "IDPlayer int NOT NULL,"+ 
                "PRIMARY KEY(IDGame, IDPlayer))";
            if (OpenConnection())
            {
                try
                {
                    comm.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    //TODO query error
                }
                CloseConnection();
            }
            else
            {
                Console.WriteLine("error opening connection");
            }
        }

        private void CreateCourtRankTable()
        {
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "CREATE VIEW courtRank AS " +
                "SELECT IDCourt, AVG(rank) as rank FROM CourtRankingsLog " +
                "GROUP BY IDCourt;";
            if (OpenConnection())
            {
                try
                {
                    comm.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    //TODO query error
                }
                CloseConnection();
            }
            else
            {
                Console.WriteLine("error opening connection");
            }
        }

        private void CreateCourtRankingsTable()
        {
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "CREATE TABLE CourtRankingsLog" +
                "(IDCourt int NOT NULL,rankerId int NOT NULL," +
                "rank int NOT NULL,text varchar(512), PRIMARY KEY (IDCourt, rankerId))";
            if (OpenConnection())
            {
                try
                {
                    comm.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    //TODO query error
                }
                CloseConnection();
            }
            else
            {
                Console.WriteLine("error opening connection");
            }
        }

        public void InitializeDB()
        {
            CreatePlayersTable();
            CreatePlayersHistoryTable();
            CreateAdminsTable();
            CreateGamesTable();
            CreateCourtsTable();
            CreateGameCourtTable();
            CreateGamePlayerTable();
            CreateCourtRankingsTable();
            CreateCourtRankTable();
            //create all tables
        }
        public void ClearDB()
        {
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "DROP DATABASE tenpasdb; CREATE DATABASE tenpasdb;";
            if (OpenConnection())
            {
                try
                {
                    comm.ExecuteNonQuery();
                }
                catch
                {
                    //TODO query error
                }
                connection.Close();
            }
            else
            {
                Console.WriteLine("error opening connection");
            }
        }
        #endregion

        #region games

        public List<Game> FindGames(Dictionary<string, string> param)
        {
            string where = "";
            string c="";
            List<Game> games = new List<Game>();
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "SELECT games.ID,players.FirstName,players.LastName,date,time,numberOfPlayers," +
                "private,duration FROM games JOIN players ON games.creator = players.ID";
            string value;
            if (param.TryGetValue("date", out value))
            {
                if (where.Equals(string.Empty))
                {
                    where += " WHERE ";
                } else
                {
                    where += " and ";
                }
                where += "games.date = ?date";
                comm.Parameters.AddWithValue("?date", value);
            }
            if (param.TryGetValue("city", out value))
            {
                c += value;
                if (where.Equals(string.Empty))
                {
                    where += " WHERE ";
                }
                else
                {
                    where += " and ";
                }
                where += "games.ID in (SELECT IDGame from gamecourt where IDcourt in (SELECT ID from courts where city = ?city))";
                comm.Parameters.AddWithValue("?city", value);
            }
            if (param.TryGetValue("court", out value))
            {
                if (where.Equals(string.Empty))
                {
                    where += " WHERE ";
                }
                else
                {
                    where += " and ";
                }
                where += "games.ID in (SELECT IDGame from gamecourt where IDcourt in (SELECT ID from courts where name = ?court))";
                comm.Parameters.AddWithValue("?court", value);
            }
            if (param.TryGetValue("duration", out value))
            {
                if (where.Equals(string.Empty))
                {
                    where += " WHERE ";
                }
                else
                {
                    where += " and ";
                }
                where += "games.duration = ?duration";
                comm.Parameters.AddWithValue("?duration", value);
            }
            if (where.Equals(string.Empty))
            {
                where += " WHERE ";
            }
            else
            {
                where += " and ";
            }
            where += "games.private = 0";
            comm.Parameters.AddWithValue("?duration", value);
            Console.WriteLine(c);
            comm.CommandText += where += ";";
            Boolean filterTime = false;
            int minHour = 0, minMinute = 0;
            if (param.TryGetValue("time", out value))
            {
                filterTime = true;
                minHour = Int32.Parse(value.Split(':')[0]);
                if (minHour == 24)
                {
                    minHour = 0;
                }
                minMinute = Int32.Parse(value.Split(':')[1]);
            } 


            MySqlDataReader result;
            if (OpenConnection())
            {
                try
                {
                    result = comm.ExecuteReader();
                }
                catch (Exception e)
                {
                    result = null;
                    //TODO query exception
                }
            }
            else
            {
                return null;
            }

            //checks if there are games
            if (result != null && result.Read())
            {
                int uid = 0;
                int duration = 0;
                string first = "";
                string last = "";
                string date = "";
                string time = "";
                bool priv = false;
                int numPlayers = 0;

                do
                {
                    try
                    {
                        uid = (int)result["ID"];
                        duration = (int)result["duration"];
                        first = (string)result["FirstName"];
                        last = (string)result["LastName"];
                        date = (string)result["date"];
                        time = (string)result["time"];
                        priv = (bool)result["private"];
                        numPlayers = (int)result["numberOfPlayers"];
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    Boolean shouldAdd = true;
                    if (filterTime) {
                        int hour = Int32.Parse(time.Split(':')[0]);
                        int minutes = Int32.Parse(time.Split(':')[1]);
                        if (hour == 24)
                        {
                            hour = 0;
                        }
                        if (hour == minHour)
                        {
                            if (minutes < minMinute)
                            {
                                shouldAdd = false;
                            }
                        } else if (hour < minHour)
                        {
                            shouldAdd = false;
                        }
                    }

                    if (shouldAdd)
                        games.Add(new Game((uint)uid, first + " " + last, date, time, numPlayers, priv, (uint)duration));
                } while (result.Read());
                CloseConnection();
            }
            else if (result == null)
            {
                return null;
            }
            return games;
        }

        public bool addPlayerToGame(string gid, string uid)
        {
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "INSERT INTO gameplayer (IDGame, IDPlayer) values (?gid,?uid);";
            comm.Parameters.AddWithValue("?uid", Int32.Parse(uid));
            comm.Parameters.AddWithValue("?gid", Int32.Parse(gid));
            if (OpenConnection())
            {
                try
                {
                    comm.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    if (e.Message.Contains("Duplicate entry"))
                        return true;
                    else
                        return false;
                    
                }
                finally
                {
                    CloseConnection();
                }
                return true;
            }
            else
            {
                return false;
            }
        }


        public Game GetGameFromDB(string id)
        {
            Game game = null;
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "SELECT username as creator, creator as creatorID, date, time,"+
                "numberOfPlayers, private, duration, games.addedDate,courts.ID as courtID, name as courtName," +
                "courts.city, street, streetNum FROM ((games JOIN  gamecourt ON games.ID = gamecourt.IDGame) "+
                "JOIN courts ON courts.ID = gamecourt.IDcourt) JOIN players ON games.creator = players.ID WHERE"+
                " games.ID = ?Id;";
            comm.Parameters.AddWithValue("?Id", Int32.Parse(id));
            MySqlDataReader result;
            if (OpenConnection())
            {
                try
                {
                    result = comm.ExecuteReader();
                }
                catch (Exception e)
                {
                    result = null;
                    //TODO query exception
                }
            }
            else
            {
                return null;
            }

            //checks if there are games
            if (result != null && result.Read())
            {
                string creator = "";
                int creatorID = 0;
                string date = "";
                string time = "";
                int numOfPlayers = 0;
                bool priv = false;
                int duration = 0;
                string addedDate = "";
                int courtID = 0;
                string courtName = "";
                string courtAddress = "";

                do
                {
                    try
                    {
                        creator = (string)result["creator"];
                        creatorID = (int)result["creatorID"];
                        date = (string)result["date"];
                        time = (string)result["time"];
                        numOfPlayers = (int)result["numberOfPlayers"];
                        duration = (int)result["duration"];
                        priv = (bool)result["private"];
                        addedDate = (string)result["addedDate"];
                        courtID = (int)result["courtID"];
                        courtName = (string)result["courtName"];
                        string street = (string)result["street"];
                        courtAddress = street + " " + ((int)result["streetNum"]).ToString()+ ", " +
                            (string)result["city"]; ;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    CloseConnection();
                    comm = connection.CreateCommand();
                    comm.CommandText = "SELECT * from players where ID in (SELECT IDPlayer from gameplayer where IDGAME =?Id);";
                    comm.Parameters.AddWithValue("?Id", Int32.Parse(id));
                    if (OpenConnection())
                    {
                        try
                        {
                            result = comm.ExecuteReader();
                        }
                        catch (Exception e)
                        {
                            result = null;
                            //TODO query exception
                        }
                    }
                    else
                    {
                        return null;
                    }
                    List<Player> players = new List<Player>();
                    if (result != null)
                    {
                        
                        while (result.Read())
                        {
                            int uid = 0;
                            string username = "";
                            string email = "";
                            string firstname = "";
                            string lastname = "";
                            string birthdate = "";
                            string city = "";
                            try
                            {
                                username = (string)result["Username"];
                                email = (string)result["Email"];
                                firstname = (string)result["FirstName"];
                                lastname = (string)result["LastName"];
                                birthdate = (string)result["Birthday"];
                                city = (string)result["City"];
                                uid = (int)result["ID"];
                            }
                            catch (Exception e)
                            {
                                Console.WriteLine(e.Message);
                            }

                            Player p = new Player(uid, username, "0", email, "",
                                    firstname, lastname,
                                    birthdate, city);
                            players.Add(p);
                        }
                    }
                    
                    game = new Game(UInt32.Parse(id), creator, date, time, numOfPlayers, priv, (uint)duration, addedDate, courtAddress,courtName, (uint)courtID, (uint)creatorID, players);
                } while (result.Read());
                
            }
            else if (result == null)
            {
                return null;
            }
            return game;
        }

        public List<Game> GetGamesFromDB(string id)
        {
            List<Game> games = new List<Game>();
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "SELECT games.ID,players.FirstName,players.LastName,date,time,numberOfPlayers," +
                "private,duration FROM games JOIN players ON games.creator = players.ID " +
                "WHERE games.ID in (SELECT IDGame from gameplayer WHERE IDPlayer = ?Id);";

            comm.Parameters.AddWithValue("?Id", Int32.Parse(id));

            MySqlDataReader result;
            if (OpenConnection())
            {
                try
                {
                    result = comm.ExecuteReader();
                }
                catch (Exception e)
                {
                    result = null;
                    //TODO query exception
                }
            }
            else
            {
                return null;
            }

            //checks if there are games
            if (result != null && result.Read())
            {
                int uid = 0;
                int duration = 0;
                string first = "";
                string last = "";
                string date = "";
                string time = "";
                bool priv = false;
                int numPlayers = 0;

                do
                {
                    try
                    {
                        uid = (int)result["ID"];
                        duration = (int)result["duration"];
                        first = (string)result["FirstName"];
                        last = (string)result["LastName"];
                        date = (string)result["date"];
                        time = (string)result["time"];
                        priv = (bool)result["private"];
                        numPlayers = (int)result["numberOfPlayers"];
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    games.Add(new Game((uint)uid, first + " " + last, date, time, numPlayers, priv, (uint)duration));
                } while (result.Read());
                CloseConnection();
            }
            else if (result == null)
            {
                return null;
            }
            return games;
        }

        public List<Game> GetAllGamesFromDB()
        {
            List<Game> games = new List<Game>();
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "SELECT games.ID,players.FirstName,players.LastName,date,time,numberOfPlayers," +
                "private,duration FROM games JOIN players ON games.creator = players.ID;";
            MySqlDataReader result;
            if (OpenConnection())
            {
                try
                {
                    result = comm.ExecuteReader();
                }
                catch (Exception e)
                {
                    result = null;
                    //TODO query exception
                }
            }
            else
            {
                return null;
            }

            //checks if there are games
            if (result != null && result.Read())
            {
                int uid = 0;
                int duration = 0;
                string first = "";
                string last = "";
                string date = "";
                string time = "";
                bool priv = false;
                int numPlayers = 0;

                do
                {
                    try
                    {
                        uid = (int)result["ID"];
                        duration = (int)result["duration"];
                        first = (string)result["FirstName"];
                        last = (string)result["LastName"];
                        date = (string)result["date"];
                        time = (string)result["time"];
                        priv = (bool)result["private"];
                        numPlayers = (int)result["numberOfPlayers"];
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    games.Add(new Game((uint)uid, first + " " + last, date, time, numPlayers, priv, (uint)duration));
                } while (result.Read());
                CloseConnection();
            }
            else if (result == null)
            {
                return null;
            }
            return games;
        }

        public List<Game> GetAllCourtsGamesFromDB(string id)
        {
            List<Game> games = new List<Game>();
            MySqlCommand comm = connection.CreateCommand();
            comm.CommandText = "SELECT * FROM games JOIN gamecourt ON games.ID = gamecourt.IDGame WHERE IDcourt = ?Id;";

            comm.Parameters.AddWithValue("?Id", Int32.Parse(id));

            MySqlDataReader result;
            if (OpenConnection())
            {
                try
                {
                    result = comm.ExecuteReader();
                }
                catch (Exception e)
                {
                    result = null;
                    //TODO query exception
                }
            }
            else
            {
                return null;
            }

            //checks if there are games
            if (result != null && result.Read())
            {
                int uid = 0;
                int duration = 0;
                string date = "";
                string time = "";
                bool priv = false;
                int numPlayers = 0;

                do
                {
                    try
                    {

                        uid = (int)result["ID"];
                        duration = (int)result["duration"];
                        date = (string)result["date"];
                        time = (string)result["time"];
                        priv = (bool)result["private"];
                        numPlayers = (int)result["numberOfPlayers"];
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    games.Add(new Game((uint)uid,"", date, time, numPlayers, priv, (uint)duration));
                } while (result.Read());
                CloseConnection();
            }
            else if (result == null)
            {
                return null;
            }
            return games;
        }



        #endregion

    }

}
