﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPServer
{
     public class Rank
    {
        #region members
        public int Total
        {
            get
            {
                return CalculateTotalRank();
            }
        }
        #region creterias
        private int creteria1;
        public int Creteria1
        {
            get
            {
                return creteria1;
            }
        }
        private int creteria2;
        public int Creteria2
        {
            get
            {
                return creteria2;
            }
        }
        private int creteria3;
        public int Creteria3
        {
            get
            {
                return creteria3;
            }
        }
        private int creteria4;
        public int Creteria4
        {
            get
            {
                return creteria4;
            }
        }
        #endregion
        #endregion

        #region methods
        public Rank()
        {
            creteria1 = creteria2 = creteria3 = creteria4 = 0;
        }

        private int CalculateTotalRank()
        {
            throw new NotImplementedException();
        }

        override
        //Need to implement with map
        public string ToString()
        {
            throw new NotImplementedException();
        }
        #region UpdateRankings
        public void Creterita1Up()
        {
            creteria1++;
        }

        public void Creterita2Up()
        {
            creteria2++;
        }

        public void Creterita3Up()
        {
            creteria3++;
        }

        public void Creterita4Up()
        {
            creteria4++;
        }

        public void Creterita1Down()
        {
            creteria1--;
        }

        public void Creterita2Down()
        {
            creteria2--;
        }

        public void Creterita3Down()
        {
            creteria3--;
        }

        public void Creterita4Down()
        {
            creteria4--;
        }
        #endregion

        #endregion

    }
}
