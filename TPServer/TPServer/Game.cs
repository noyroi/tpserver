﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPServer
{
    public class Game
    {
        public Game(uint uid,string name, string date, string Time, int NumOfPlayers,
            bool Priv, uint Duration, string addedDate, string courtAddress, string courtName, uint courtID, uint creatorID,List<Player> players)
        {
            this.uid = uid;
            this.Name = name;
            this.date = date;
            this.Time = Time;
            this.NumOfPlayers = NumOfPlayers;
            this.Priv = Priv;
            this.Duration = Duration;
            this.addedDate = addedDate;
            this.CourtAddress = courtAddress;
            this.CourtName = courtName;
            this.CourtId = courtID;
            this.CreatorId = creatorID;
            this.players = players;
        }
        public Game(uint uid, string name, string date, string Time, int NumOfPlayers,
            bool Priv, uint Duration)
        {
            this.uid = uid;
            this.Name = name;
            this.date = date;
            this.Time = Time;
            this.NumOfPlayers = NumOfPlayers;
            this.Priv = Priv;
            this.Duration = Duration;
            //not in use
            this.addedDate = "";
            this.CourtAddress = "";
            this.CourtName = "";
            this.CourtId = 0;
            this.CreatorId = 0;

        }
        public List<Player> players
        {
            get;
        }
        public uint uid
        {
            get;
        }
        public string Name
        {
            get;
        }

        public string date;
        public DateTime Date
        {
            get
            {
                String[] splitdate = date.Split('/');
                int day = Int32.Parse(splitdate[0]), month = Int32.Parse(splitdate[1]), year = Int32.Parse(splitdate[2]);
                return new DateTime(year, month, day);
            }
        }
        public string addedDate;
        public DateTime AddedDate
        {
            get
            {
                String[] splitdate = addedDate.Split('/');
                int day = Int32.Parse(splitdate[0]), month = Int32.Parse(splitdate[1]), year = Int32.Parse(splitdate[2]);
                return new DateTime(year, month, day);
            }
        }
        public string Time
        {
            get;
        }
        public int NumOfPlayers
        {
            get;
        }
        public bool Priv
        {
            get;
        }
        public uint Duration
        {
            get;
        }
        public string CourtName { get; }
        public string CourtAddress { get; }
        public uint   CourtId { get; }
        public uint CreatorId { get; }
    }
}
