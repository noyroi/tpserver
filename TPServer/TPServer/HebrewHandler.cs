﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TPServer
{
    class HebrewHandler
    {
        public static void WriteLine(String input)
        {
            Console.WriteLine(new string(input.Reverse().ToArray()));
        }
    }
}
