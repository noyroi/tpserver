﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace TPServer
{
    public class User
    {
        #region members

        public string Username
        {
            get;
        }

        public string Password {
            get;
        }

        public string Email
        {
            get;
        }

        #endregion
        public User()
        {

        }
        public User(string username, string password, string email)
        {
            Username = username;
            this.Password = password;
            Email = email;
        }
    }
}
